package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.SessionRepository;
import ru.tsc.golovina.tm.repository.UserRepository;
import ru.tsc.golovina.tm.util.HashUtil;

public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userLogin";

    @NotNull
    private final String userPassword = "userPassword";

    @NotNull
    private Session session;

    public SessionServiceTest() throws AbstractException {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        user.setPassword(HashUtil.salt(secret, iteration, userPassword));
        userService.add(user);
    }

    @Before
    public void initializeTest() throws AbstractException {
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void close() {
        Assert.assertEquals(userId, session.getUserId());
        Assert.assertTrue(sessionService.close(session));
        Assert.assertFalse(sessionService.close(session));
    }

    @Test
    public void validate() throws AbstractException {
        sessionService.validate(session);
    }

    @After
    public void finalizeTest() {
        sessionService.close(session);
    }

}

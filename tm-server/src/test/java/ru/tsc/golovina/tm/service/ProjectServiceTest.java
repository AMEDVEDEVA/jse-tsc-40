package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.repository.ProjectRepository;

public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @NotNull
    private final String userId = "userId";

    public ProjectServiceTest() throws AbstractException {
        project = new Project();
        projectId = project.getId();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);
        projectService = new ProjectService(connectionService);
        projectService.add(userId, project);
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        @NotNull final Project newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertEquals(newProject.getName(), newProjectName);
        Assert.assertEquals(newProject.getDescription(), newProjectDescription);
    }

    @Test
    public void findByName() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final Project project = projectService.findByName(userId, projectName);
        Assert.assertEquals(project.getName(), projectName);
        Assert.assertEquals(project.getDescription(), projectDescription);
        Assert.assertEquals(project.getUserId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findByIndex(userId, 0));
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startById(userId, projectId);
        Assert.assertNotNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(userId, 0);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByIndex(userId, 0);
        Assert.assertNotNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByName(userId, projectName);
        Assert.assertNotNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETED);
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

}

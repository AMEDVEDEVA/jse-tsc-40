package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTask";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String userId;

    public TaskRepositoryTest() {
        taskRepository = new TaskRepository();
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPassword(HashUtil.salt( "qwe", 3,"guest"));
        task = new Task();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        projectRepository = new ProjectRepository();
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
    }

    @Before
    public void initializeTest() throws AbstractException {
        taskRepository.add(task);
        projectRepository.add(project);
    }

    @Test
    public void findTask() throws AbstractException {
        Assert.assertEquals(task, taskRepository.findByName(userId, taskName));
        Assert.assertEquals(task, taskRepository.findById(taskId));
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(0));
    }

    @Test
    public void removeByName() throws AbstractException {
        Assert.assertNotNull(task);
        taskRepository.removeByName(userId, taskName);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void startById() throws AbstractException {
        taskRepository.startById(userId, taskId);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        taskRepository.startByIndex(userId, 0);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        taskRepository.startByName(userId, taskName);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        taskRepository.finishById(userId, taskId);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        taskRepository.finishByIndex(userId, 0);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        taskRepository.finishByName(userId, taskName);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProjectById(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskById(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskById(userId, taskId);
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void findAllByProjectId(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(task, taskRepository.findAllTaskByProjectId(userId, projectId).get(0));
    }

    @Test
    public void removeAllTaskByProjectId(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final Task task1 = new Task();
        task1.setUserId(userId);
        taskRepository.add(userId, task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void finalizeTest() {
        taskRepository.clear(userId);
        projectRepository.clear(userId);
    }

}

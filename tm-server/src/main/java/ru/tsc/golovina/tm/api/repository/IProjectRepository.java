package ru.tsc.golovina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Nullable
    @Select("SELECT * FROM projects WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Project existById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM projects WHERE user_id = #{userId};")
    void clearById(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM projects WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM projects WHERE user_id = #{userId} AND id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByUserIdAndProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM projects where user_id = #{userId} AND id = #{id}")
    void removeByUserIdAndProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Insert("INSERT INTO projects" +
            " (id, user_id, name, description, status, create_date, start_date)" +
            " VALUES (#{project.id}, #{project.userId}, #{project.name}, #{project.description}," +
            " #{project.status}, #{project.createDate}, #{project.startDate})")
    void add(@NotNull @Param("project") Project project);

    @Nullable
    @Select("SELECT * FROM projects" +
            " WHERE user_id = #{userId} AND name = #{name}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Nullable
    @Select("SELECT * FROM projects" +
            " WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByIndex(
            @NotNull @Param("userId") String userId,
            @Param("index") int index
    );

    @Delete("DELETE FROM projects WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE projects SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE projects SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name};")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void updateStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Delete("DELETE FROM projects")
    void clear();

    @Nullable
    @Select("SELECT * FROM projects")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT * FROM projects WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM projects where id = #{id}")
    void removeById(@NotNull @Param("id") String id);

}

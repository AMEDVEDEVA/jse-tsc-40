package ru.tsc.golovina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String DEFAULT_SERVER_PORT = "8080";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private static final String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_DEFAULT_VALUE = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private static final String JDBC_LOGIN_KEY = "jdbc.user";

    @NotNull
    private static final String JDBC_LOGIN_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String JDBC_PASSWORD_KEY = "jdbc.password";

    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getString(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name))
            return System.getProperty(name);
        if (System.getenv().containsKey(name))
            return System.getenv(name);
        return properties.getProperty(name, defaultValue);
    }

    public Integer getInteger(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) {
            final String value = System.getProperty(name);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(name)) {
            final String value = System.getenv(name);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(name, defaultValue);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getString(SERVER_HOST, DEFAULT_SERVER_HOST);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getString(SERVER_PORT, DEFAULT_SERVER_PORT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getString(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getInteger(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionSecret() {
        return getString(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionIteration() {
        return getInteger(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getBackupInterval() {
        return getInteger(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getString(JDBC_URL_KEY, JDBC_URL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcLogin() {
        return getString(JDBC_LOGIN_KEY, JDBC_LOGIN_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getString(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT_VALUE);
    }

}

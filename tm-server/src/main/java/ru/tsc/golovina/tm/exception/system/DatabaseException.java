package ru.tsc.golovina.tm.exception.system;

import ru.tsc.golovina.tm.exception.AbstractException;

public class DatabaseException extends AbstractException {

    public DatabaseException() {
        super("Database command execution exception");
    }

}

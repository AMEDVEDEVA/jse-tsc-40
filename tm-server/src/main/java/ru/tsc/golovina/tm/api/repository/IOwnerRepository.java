package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    List<E> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator) throws SQLException;

    @Nullable
    E add(@NotNull String userId, @NotNull E entity) throws SQLException;

    @Nullable
    E findById(@NotNull String userId, @NotNull String id) throws SQLException;

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    void clear(@NotNull String userId) throws SQLException;

    @NotNull
    E removeById(@NotNull String userId, @NotNull String id) throws SQLException;

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    void remove(@NotNull String userId, @NotNull E entity) throws SQLException;

    @NotNull
    Integer getSize(@NotNull String userId) throws SQLException;

    boolean existsByName(@Nullable String userId, @NotNull String name) throws SQLException;

    @NotNull E findByName(@NotNull String userId, @NotNull String name) throws SQLException;

    @Nullable E startById(@NotNull String userId, @NotNull String id) throws SQLException;

    @NotNull E startByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    @NotNull E startByName(@NotNull String userId, @NotNull String name) throws SQLException;

    @Nullable E finishById(@NotNull String userId, @NotNull String id) throws SQLException;

    @NotNull E finishByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    @NotNull E finishByName(@NotNull String userId, @NotNull String name) throws SQLException;

    @Nullable E changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws SQLException;

    @NotNull E changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) throws SQLException;

    @NotNull E changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) throws SQLException;

    @NotNull E removeByName(@Nullable String userId, @Nullable String name) throws SQLException;

}

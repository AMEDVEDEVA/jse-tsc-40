package ru.tsc.golovina.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

import java.sql.Timestamp;

import static org.reflections.util.Utils.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ISessionRepository.class);
    }

    public SessionService(@NotNull IConnectionService connectionService, @NotNull final IUserService userService,
                          @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public boolean open(@NotNull final String login, @NotNull final String password) throws JsonProcessingException {
        @NotNull final Session session = new Session();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getPasswordIteration();
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final String hash = HashUtil.salt(secret, iteration, password);
            if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
            session.setUserId(user.getId());
            session.setTimestamp(new Timestamp());
            session.setSignature(null);
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            sessionRepository.open(sign(session));
            sqlSession.commit();
        }
    }

    @SneakyThrows
    @Override
    @NotNull
    public Session sign(@NotNull final Session session) {
            session.setSignature(null);
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getSessionIteration();
            @NotNull String secret = propertyService.getSessionSecret();
            @Nullable final String signature = HashUtil.salt(secret, iteration, session.getPasswordHash());
            session.setSignature(signature);
            return session;
        }

    @Override
    public boolean close(@NotNull Session session) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            int res = sessionRepository.close(session);
            sqlSession.commit();
            return res > 0;
        }
    }

    @Override
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final User user = userService.findUserByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPassword());
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            if (sessionRepository.contains(session.getId()) == null) throw new AccessDeniedException();
        }
    }

    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final String userId = session.getUserId();
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            @NotNull final Role userRole = user.getRole();
            if (!userRole.equals(role)) throw new AccessDeniedException();
        }
    }

}

package ru.tsc.golovina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.model.Project;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    public IProjectRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IProjectRepository.class);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.add(project);
            session.commit();
        }
        return project;
    }

}
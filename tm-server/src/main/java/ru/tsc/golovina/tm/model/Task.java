package ru.tsc.golovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractOwnerEntity implements IWBS {

    @Nullable
    private String projectId = null;

    public Task(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

}

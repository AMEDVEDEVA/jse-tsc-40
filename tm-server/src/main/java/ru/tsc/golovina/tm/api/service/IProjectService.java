package ru.tsc.golovina.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;

import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    @NotNull IProjectRepository getRepository(@NotNull SqlSession session);

    @NotNull Project create(@Nullable String userId, @Nullable String name, @Nullable String description);
}

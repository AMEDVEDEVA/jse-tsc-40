package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.exception.empty.EmptyIndexException;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.add(entity);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new EntityNotFoundException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.addAll(entities);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public List<E> findAll() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll(comparator);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void clear() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) {
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findByIndex(index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsById(id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsByIndex(index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E removeById(final @NotNull String id) {
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            entity = repository.removeById(id);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            entity = repository.removeByIndex(index);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public Integer getSize() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.getSize();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

}

package ru.tsc.golovina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.api.service.IPropertyService;
import ru.tsc.golovina.tm.api.service.*;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.exception.system.UnknownCommandException;
import ru.tsc.golovina.tm.repository.*;
import ru.tsc.golovina.tm.service.*;
import ru.tsc.golovina.tm.util.HashUtil;
import ru.tsc.golovina.tm.util.SystemUtil;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

@Getter
public class Bootstrap implements IEndpointLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.golovina.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsc.golovina.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String... args) {
        displayWelcome();
        runArgs(args);
        initCommands();
        fileScanner.init();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private void runArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(args[0]);
        command.execute();
    }

    public void runCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException(command);
        final Role[] roles = abstractCommand.roles();
        abstractCommand.execute();
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setIServiceLocator(this);
        commandService.add(command);
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}

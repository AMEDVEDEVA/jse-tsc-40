package ru.tsc.golovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getUserEndpoint().findUserByLogin(session, login).getId();
        @Nullable final String currentUserId = session.getUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getAdminUserEndpoint().lockByLoginUser(session, login);
    }

}

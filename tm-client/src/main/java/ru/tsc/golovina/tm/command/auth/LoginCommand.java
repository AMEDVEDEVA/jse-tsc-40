package ru.tsc.golovina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login user to system";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session = serviceLocator.getSessionEndpoint().openSession(login, password);
        serviceLocator.setSession(session);
        System.out.println("Welcome to task manager");
    }

}

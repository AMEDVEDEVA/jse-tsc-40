package ru.tsc.golovina.tm.exception.entity;

import ru.tsc.golovina.tm.exception.AbstractException;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(final String login) {
        super("Error. User with login `" + login + "` already exists.");
    }

}

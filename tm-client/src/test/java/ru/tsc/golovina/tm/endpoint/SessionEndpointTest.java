package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.endpoint.SessionEndpoint;
import ru.tsc.golovina.tm.endpoint.SessionEndpointService;
import ru.tsc.golovina.tm.exception.AbstractException;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openCloseSession() throws AbstractException {
        @NotNull final Session sessionTemp = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
    }

    @Test
    @Category(SoapCategory.class)
    public void register() throws AbstractException {
        @NotNull final Session sessionTemp = sessionEndpoint.register("temp", "temp", "a@a.com");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
    }

}
